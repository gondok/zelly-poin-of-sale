package com.gondok.connect.model.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author Hendro yuwono
 */
@Data
@Builder
public class SuplierResponse {
    private String id;
    private String name;
    private Address address;
    private List<Product> products;

    @Data
    @Builder
    public static class Address {
        private String detailAddress;
        private String district;
        private String city;
        private String province;
    }

    @Data
    @Builder
    public static class Product {
        private int quantity;
        private String motive;
    }
}
