package com.gondok.connect.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author Hendro yuwono
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SuplierRequest {
    @NotEmpty
    private String name;
}
