package com.gondok.connect.model.request;

import lombok.Data;

/**
 * @author Hendro yuwono
 */
@Data
public class SuplierProductRequest {
    private String motive;
    private String description;
    private Integer count;
}
