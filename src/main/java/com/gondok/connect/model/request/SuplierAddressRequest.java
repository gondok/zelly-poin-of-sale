package com.gondok.connect.model.request;

import lombok.Data;

/**
 * @author Hendro yuwono
 */
@Data
public class SuplierAddressRequest {
    private String address;
    private String district;
    private String city;
    private String province;
}
