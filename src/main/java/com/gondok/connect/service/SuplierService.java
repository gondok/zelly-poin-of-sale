package com.gondok.connect.service;

import com.gondok.connect.entity.Suplier;
import com.gondok.connect.model.request.SuplierAddressRequest;
import com.gondok.connect.model.request.SuplierProductRequest;
import com.gondok.connect.model.request.SuplierRequest;
import com.gondok.connect.model.response.SuplierResponse;
import com.gondok.connect.repository.SuplierRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hendro yuwono
 */
@Service
@Slf4j
public class SuplierService {

    @Autowired
    private SuplierRepository suplierRepository;

    public Flux<SuplierResponse> findAll() {
        return suplierRepository.findAll()
                .switchIfEmpty(responseException())
                .map(this::toResponse)
                .log();
    }

    public Mono<SuplierResponse> findById(String id) {
        return suplierRepository.findById(id)
                .switchIfEmpty(responseException())
                .map(this::toResponse)
                .log();
    }

    private SuplierResponse toResponse(Suplier suplier) {

        return SuplierResponse.builder()
                .id(suplier.getId())
                .name(suplier.getName())
                .address(suplier.getAddress() == null ? null : toAddressResponse(suplier.getAddress()))
                .products(suplier.getProducts().isEmpty() ? null : toProductResponse(suplier.getProducts()))
                .build();
    }

    private SuplierResponse.Address toAddressResponse(Suplier.Address address) {
        return SuplierResponse.Address.builder()
                .detailAddress(address.getAddress())
                .city(address.getCity())
                .district(address.getDistrict())
                .province(address.getProvince())
                .build();
    }

    private List<SuplierResponse.Product> toProductResponse(List<Suplier.Product> products) {
        return products.stream().map(this::toProduct).collect(Collectors.toList());
    }

    private SuplierResponse.Product toProduct(Suplier.Product product) {
        return SuplierResponse.Product.builder()
                .motive(product.getMotive())
                .quantity(product.getCount())
                .build();
    }

    public Mono<Void> save(SuplierRequest request) {
        Suplier suplier = Suplier.builder()
                .name(request.getName())
                .build();
        return suplierRepository.save(suplier).then().log();
    }

    public Mono<SuplierResponse> edit(String id, SuplierRequest request) {
        return suplierRepository.findById(id)
                .switchIfEmpty(responseException())
                .map(v -> {
                    v.setName(request.getName());
                    return v;
                })
                .flatMap(v -> suplierRepository.save(v))
                .map(this::toResponse)
                .log();
    }

    public Mono<SuplierResponse> addAddress(String id, SuplierAddressRequest request) {
        return suplierRepository.findById(id)
                .switchIfEmpty(responseException())
                .map(v -> addNewAddressInSuplier(v, request))
                .flatMap(suplierRepository::save)
                .map(this::toResponse)
                .log();
    }

    private Suplier addNewAddressInSuplier(Suplier suplier, SuplierAddressRequest request) {
        Suplier.Address address = Suplier.Address.builder()
                .address(request.getAddress())
                .city(request.getCity())
                .district(request.getDistrict())
                .province(request.getProvince())
                .build();

        suplier.setAddress(address);
        return suplier;
    }

    public Mono<SuplierResponse> saveProduct(String id, SuplierProductRequest request) {
        return suplierRepository.findById(id)
                .switchIfEmpty(responseException())
                .map(suplier -> addOrUpdateProductInSuplier(suplier, request))
                .flatMap(suplierRepository::save)
                .map(this::toResponse)
                .log();
    }

    public Mono<SuplierResponse> deleteProduct(String id, String productId) {
        return suplierRepository.findById(id)
                .map(suplier -> findProductAndRemoveIt(suplier, productId))
                .flatMap(suplierRepository::save)
                .map(this::toResponse);
    }

    private Suplier findProductAndRemoveIt(Suplier suplier, String productId) {
        Suplier.Product product = findProductInSuplier(suplier, productId);
        return removeProductFromSuplier(suplier, product);
    }

    private Suplier removeProductFromSuplier(Suplier suplier, Suplier.Product product) {
        suplier.getProducts().remove(product);
        return suplier;
    }

    private Suplier.Product findProductInSuplier(Suplier suplier, String productId) {
        return suplier.getProducts().stream()
                .filter(v -> v.getMotive().equalsIgnoreCase(productId)).findFirst().get();
    }

    private Suplier addOrUpdateProductInSuplier(Suplier suplier, SuplierProductRequest request) {
        if (isSuplierContainProduct(suplier, request)) {
            incrementProductQuantity(suplier, request);
        } else {
            addNewProductToSuplier(suplier, request);
        }

        return suplier;
    }

    private void addNewProductToSuplier(Suplier suplier, SuplierProductRequest request) {
        Suplier.Product product = Suplier.Product.builder()
                .motive(request.getMotive())
                .active(true)
                .description(request.getDescription())
                .count(request.getCount())
                .build();

        suplier.getProducts().add(product);
    }

    private void incrementProductQuantity(Suplier suplier, SuplierProductRequest request) {
        suplier.getProducts().stream()
                .filter(product -> product.getMotive().equalsIgnoreCase(request.getMotive()))
                .forEach(product -> product.setCount(product.getCount() + request.getCount()));
    }

    private boolean isSuplierContainProduct(Suplier suplier, SuplierProductRequest request) {
        return suplier.getProducts().stream()
                .anyMatch(product -> product.getMotive().equalsIgnoreCase(request.getMotive()));
    }

    private <T> Mono<T> responseException() {
        return Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND, "id not found"));
    }
}
