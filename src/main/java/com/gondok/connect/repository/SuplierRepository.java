package com.gondok.connect.repository;

import com.gondok.connect.entity.Suplier;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

/**
 * @author Hendro yuwono
 */
public interface SuplierRepository extends ReactiveMongoRepository<Suplier, String> {

}
