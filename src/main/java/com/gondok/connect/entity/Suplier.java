package com.gondok.connect.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Hendro yuwono
 */
@Data
@Builder
@Document
public class Suplier {

    @Id
    private String id;

    private String name;

    private Address address;

    private List<Product> products;


    @Data
    @Builder
    public static class Address {
        private String address;

        private String district;

        private String city;

        private String province;
    }

    @Data
    @Builder
    public static class Product {
        private String motive;

        private Integer count;

        private boolean active;

        private String description;
    }

    public List<Product> getProducts() {
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }
}
