package com.gondok.connect.controller;

import com.gondok.connect.model.request.SuplierAddressRequest;
import com.gondok.connect.model.request.SuplierProductRequest;
import com.gondok.connect.model.request.SuplierRequest;
import com.gondok.connect.model.response.SuplierResponse;
import com.gondok.connect.service.SuplierService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * @author Hendro yuwono
 */
@RestController
public class SuplierController {

    @Autowired
    private SuplierService suplierService;

    @GetMapping("/supplier")
    public Flux<SuplierResponse> findAll() {
        return suplierService.findAll();
    }

    @GetMapping("/supplier/{id}")
    public Mono<SuplierResponse> get(@PathVariable String id) {
        return suplierService.findById(id);
    }

    @PostMapping("/supplier")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Void> save(@RequestBody SuplierRequest request) {
        return suplierService.save(request);
    }

    @PutMapping("/supplier/{id}")
    public Mono<SuplierResponse> edit(@PathVariable("id") String id, @RequestBody SuplierRequest request) {
        return suplierService.edit(id, request);
    }

    @PostMapping("/supplier/{id}/detailAddress")
    public Mono<SuplierResponse> addOrEditAddress(@PathVariable("id") String id, @RequestBody SuplierAddressRequest request) {
        return suplierService.addAddress(id, request);
    }

    @PostMapping("/supplier/{id}/product")
    public Mono<SuplierResponse> addOrEditProduct(@PathVariable("id") String id, @RequestBody SuplierProductRequest request) {
        return suplierService.saveProduct(id, request);
    }

    @DeleteMapping("/supplier/{id}/product/{productId}")
    public Mono<SuplierResponse> removeProduct(@PathVariable("id") String id, @PathVariable("productId") String productId) {
        return suplierService.deleteProduct(id, productId);
    }
}
